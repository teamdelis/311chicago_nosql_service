package com.chicago.service;

import java.util.List;

public interface GeneralServiceApi {
    List getAll(Class c);

    List getByType(Class c, String type,String field);
}
