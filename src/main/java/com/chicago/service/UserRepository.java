package com.chicago.service;

import com.chicago.domain.User;
import org.jongo.Jongo;
import org.jongo.MongoCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;

@Service("userService")
@Transactional
public class UserRepository implements UserRepositoryApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    private Jongo jongo;
    private MongoCollection usersCollection;

    @PostConstruct
    private void onInit(){
        jongo = new Jongo(mongoTemplate.getMongoDbFactory().getLegacyDb());
        usersCollection = jongo.getCollection("users_data");
    }


    @Override
    public User add(User user) {
        usersCollection.insert(user);
        return user;
    }
}
