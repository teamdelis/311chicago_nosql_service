package com.chicago.service;

import com.chicago.domain.Request;
import com.mongodb.AggregationOptions;
import org.bson.types.ObjectId;
import org.jongo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.text.ParseException;
import java.util.*;

@Service("requestService")
@Transactional
public class RequestRepository implements RequestRepositoryApi {

    @Autowired
    private MongoTemplate mongoTemplate;

    private Jongo jongo;
    private MongoCollection requestsCollection;
    private MongoCollection requestsCollectionHistory;
    private MongoCollection usersCollection;
    private MongoCollection usersCollectionHistory;
    private MongoCollection incidents_per_phone_number;

    @PostConstruct
    private void onInit(){
        jongo = new Jongo(mongoTemplate.getMongoDbFactory().getLegacyDb());
        requestsCollection = jongo.getCollection("requests_collection");
        usersCollection = jongo.getCollection("users_data");
        incidents_per_phone_number = jongo.getCollection("incidents_per_phone_number");

        if ( !mongoTemplate.collectionExists("requests_collection_history"))
            mongoTemplate.createCollection("requests_collection_history");
        if ( !mongoTemplate.collectionExists("users_data_history"))
            mongoTemplate.createCollection("users_data_history");

        requestsCollectionHistory = jongo.getCollection("requests_collection_history");
        usersCollectionHistory = jongo.getCollection("users_data_history");
    }

    public Request getRequestById(String id) {
        Criteria criteria = Criteria.where("id").is(id);
        return (Request) mongoTemplate.find(Query.query(criteria), Request.class).get(0);

    }

    @Override
    public List<Object> query1(String date1,String date2) {

        String match = "{$match:{'Creation Date': { $gte: { '$date': # }, $lte: { '$date': # } }}}";
        String sort = "{$sortByCount: '$Type of Service Request'}";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,format(date1),format(date2))
                .and(sort)
                .as(Object.class)
                .iterator();
        return toList(rs);
    }


    @Override
    public List<Object> query2(String type,String date1,String date2) {

        String match = "{$match:{'Type of Service Request': #, 'Creation Date': { $gte: { '$date': # }, $lte: { '$date': # } }}}";
        String group = "{$group : {_id: '$Creation Date', requests_count: {$sum: 1}}}";
        String sort = "{$sort: {'_id': 1}}";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,type,format(date1),format(date2))
                .and(group)
                .and(sort)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query3(String date) {
        String match = "{" +
                "$match: {" +
                "'Creation Date': { '$date': # } ," +
                "'ZIP Code': { $exists: 1, $gt: 0 }" +
                "}" +
                "}";
        String group1 = "{$group : {_id: {zipCode: '$ZIP Code', type: '$Type of Service Request'}, count: {$sum: 1}}}";
        String sort1 = "{$sort: {'_id.zipCode': 1,'count': -1}}";
        String group2 = "{$group : {_id: {zc: '$_id.zipCode'}, types: {$push: {type: '$_id.type', count: '$count'}}}}";
        String project = "{$project: {types: {$slice: ['$types', 3]}}}";
        String sort2 = "{$sort: {'_id.zc': 1}}";


        Iterator<Object> rs =  requestsCollection
                .aggregate(match,format(date))
                .and(group1)
                .and(sort1)
                .and(group2)
                .and(project)
                .and(sort2)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query4(String type)  {
        String match = "{$match: { 'Type of Service Request': #, 'Ward': { $exists: 1, $gt: 0 }}}";
        String group = "{$group: { _id: {ward: '$Ward'},count: {$sum: 1}}}";
        String sort = "{$sort: { 'count': 1 }}";
        String limit = "{$limit: 3}";
        String project = "{$project: {_id: 0, tward: '$_id.ward',count: '$count'}}";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,type)
                .and(group)
                .and(sort)
                .and(limit)
                .and(project)
                .as(Object.class).iterator();
        return toList(rs);

    }

    @Override
    public List<Object> query5(String startDate,String endDate) {

        String match = "{ $match : { 'Creation Date': { $exists: 1, $gte: { '$date': # } , $lte: { '$date': # } }, 'Completion Date': { $exists: 1 } } }";
        String group = "{ $group : { _id : null, avg_days: { $avg: { $divide: [ { $subtract: ['$Completion Date', '$Creation Date'] }, 86400000] } } } }";
        String project = "{ $project : { _id: 0, avg_days: 1 } }";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,format(startDate),format(endDate))
                .and(group)
                .and(project)
                .as(Object.class).iterator();
        return toList(rs);

    }

    @Override
    public List<Object> query6(Float[] x1,Float[] x2,Float[] x3,Float[] x4,String date){
        String match = "{$match: { 'Creation Date': { '$date': # },'GeoPoint': { $geoWithin : { $geometry: { type: 'Polygon'," +
                "coordinates: [[ [#,#], [#,#], [#,#], [#,#], [#,#] ]]}}}}}";

        String group = "{$group : {_id: '$Type of Service Request',count: {$sum: 1}}}";
        String project = "{$project : {_id: 0,requestType: '$_id',count: {$max : '$count'}}}";
        String sort = "{$sort : {'count': -1}}";
        String limit = "{$limit: 1}";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,format(date),x1[0],x1[1],x2[0],x2[1],x3[0],x3[1],x4[0],x4[1],x1[0],x1[1])
                .and(group)
                .and(project)
                .and(sort)
                .and(limit)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query7(String date)  {

        String match = "{ $match : { 'Creation Date': { '$date': # } ,'upvotes': { $exists: 1 }} }";
        String project = "{$project : {_id : 0,'Service Request Number' : 1,'upvotes' : 1}}";
        String sort = "{$sort : {'upvotes' : -1}}";
        String limit = "{$limit: 50}";

        Iterator<Object> rs =  requestsCollection
                .aggregate(match,format(date))
                .and(project)
                .and(sort)
                .and(limit)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query8() {
        String project = "{$project : {_id : 0,'name' : 1,'address' : 1,'phone_number' : 1, numberOfRequests : { $cond: { if: { $isArray: '$upvoted_requests' }, then: { $size: '$upvoted_requests' }, else: 0 } } }}}";
        String sort = "{$sort : {'numberOfRequests' : -1}}";
        String limit = "{$limit: 50}";

        Iterator<Object> rs =  usersCollection
                .aggregate(project)
                .and(sort)
                .and(limit)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query9()  {
        String project = "{$project : {wards : '$upvoted_requests.Ward'}}";
        String unwind = "{$unwind : '$wards'}";
        String group = "{$group:{_id : { usrID: '$_id', ward: '$wards'}}}";
        String sort = "{ $sortByCount : '$_id.usrID'}";
        String limit = "{$limit: 50}";
        AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();

        Iterator<Object> rs =  usersCollection
                .aggregate(project)
                .and(unwind)
                .and(group)
                .and(sort)
                .and(limit)
                .options(options)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query10()  {
        String project1 = "{$project : {'name' : 1,'phone_number' : 1, incidents :  '$upvoted_requests._id'  }}}";
        String unwind = "{$unwind : '$incidents'}";
        String group = "{$group : {" +
                "_id : { phoneNumber: '$phone_number', incidentID: '$incidents' }," +
                "names : { $addToSet: '$name' }}}";
        String project2 = "{$project : {names : '$names',count : { $size: '$names' }}}";
        String match = "{$match: {'count' : { $gt: 1 }}}";
        String sort = "{$sort : {'count' : -1}}";
        AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();

        Iterator<Object> rs =  usersCollection
                .aggregate(project1)
                .and(unwind)
                .and(group)
                .and(project2)
                .and(match)
                .and(sort)
                .options(options)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query11()  {
        String match = "{$match: {'count' : { $gt: 1 }}}";
        AggregationOptions options = AggregationOptions.builder().allowDiskUse(true).build();
        Iterator<Object> rs =  incidents_per_phone_number
                .aggregate(match)
                .options(options)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public List<Object> query12(String name) throws ParseException {

        String match = "{$match: {'name' : #}}";
        String project1 = "{$project : {_id : 0,wards:'$upvoted_requests.Ward'}}";
        String unwind = "{$unwind : '$wards'}";
        String sort = "{$sortByCount: '$wards'}";

        String project2 = "{$project : { _id : 0,ward : '$_id',votesCount: '$count'}}";


        Iterator<Object> rs =  usersCollection
                .aggregate(match,name)
                .and(project1)
                .and(unwind)
                .and(sort)
                .and(project2)
                .as(Object.class).iterator();
        return toList(rs);
    }

    @Override
    public void update(String userId,String requestId) {

        /*
        *
        *
        * db.users_data.aggregate([{$match: {_id: ObjectId("5c333a651e05711934ce5031")} },
        * {$project: {requests: "$upvoted_requests.Service Request Number"}}, {$unwind: "$requests"}, {$match: {"requests": "17-04614321"}}])
        *
        *
        * */

        String match1 = "{$match: {_id: #}}";
        String project1 = "{$project: {requests: '$upvoted_requests.Service Request Number'}}";
        String unwind = "{$unwind : '$requests'}";
        String match2 = "{$match: {'requests': #}}";
        String match3 = "{$match: {'Service Request Number': #}}";


        String project_noID = "{ $project: { _id: 0 } }";

        Iterator<Object> rs =  usersCollection
                .aggregate(match1,new ObjectId(userId))
                .and(project1)
                .and(unwind)
                .and(match2,requestId)
                .as(Object.class).iterator();

        Iterator<Object> request =  requestsCollection
                .aggregate(match3,requestId)
                .as(Object.class).iterator();

        Iterator<Object> user =  usersCollection
                .aggregate(match1,new ObjectId(userId))
                .and(project_noID)
                .as(Object.class).iterator();

        Iterator<Object> request_noID =  requestsCollection
                .aggregate(match3,requestId)
                .and(project_noID)
                .as(Object.class).iterator();


        if(!rs.hasNext()){
            usersCollectionHistory.insert(user.next());
            requestsCollectionHistory.insert(request_noID.next());
            usersCollection.update("{_id: #}",new ObjectId(userId)).with("{$addToSet: {upvoted_requests: #}}",request.next());
            requestsCollection.update("{'Service Request Number': #}",requestId).with("{$inc: {upvotes: 1}}");
        }

    }

    @Override
    public Request add(Request request) {
        requestsCollection.insert(request);
        return request;
    }

    public static List<Object> toList(Iterator<Object> rs) {
        List<Object> resultSet = new ArrayList<>();
        while(rs.hasNext()){
            resultSet.add(rs.next());
        }
        return resultSet;
    }


    public static String format(String dateStr) {
        return dateStr + "T00:00:00Z";
    }

}
