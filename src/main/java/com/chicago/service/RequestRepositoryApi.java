package com.chicago.service;

import com.chicago.domain.Request;

import java.text.ParseException;
import java.util.List;

public interface RequestRepositoryApi {

    public Request getRequestById(String id);

    List<Object> query1(String date1, String date2);

    List<Object> query2(String type,String date1, String date2);

    List<Object> query3(String date);

    List<Object> query4(String type);

    List<Object> query5(String startDate, String endDate);

    List<Object> query6(Float[] x1, Float[] x2, Float[] x3, Float[] x4, String date);

    List<Object> query7(String date);

    List<Object> query8();

    List<Object> query9();

    List<Object> query10();

    List<Object> query11();

    List<Object> query12(String name) throws ParseException;

    void update(String userId, String requestId);

    Request add(Request request);

}
