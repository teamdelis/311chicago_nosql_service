package com.chicago.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service("generalService")
@Transactional
public class GeneralService implements GeneralServiceApi {


    @Override
    public List getAll(Class c) {
        return null;
    }

    @Override
    public List getByType(Class c, String type,String field) {
        return null;
    }
}
