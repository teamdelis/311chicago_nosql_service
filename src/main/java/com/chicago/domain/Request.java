package com.chicago.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Document(collection = "requests_collection")
public class Request {

    @Id
    public String id;

    @Field("Creation Date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date creationDate;
    @Field("Status")
    private String status;
    @Field("Completion Date")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date completionDate;
    @Field("Service Request Number")
    private String serviceRequestNumber;
    @Field("Type of Service Request")
    private String requestType;
    @Field("Vehicle Make/Model")
    private String model;
    @Field("Vehicle Color")
    private String color;
    @Field("License Plate")
    private String licensePlate;
    @Field("How Many Days Has the Vehicle Been Reported as Parked?")
    private String daysParked;
    @Field("Street Address")
    private String address;
    @Field("ZIP Code")
    private String zipCode;
    @Field("X Coordinate")
    private String latitude;
    @Field("Y Coordinate")
    private String longitude;
    @Field("Ward")
    private String ward;
    @Field("Police District")
    private String policeDistrict;
    @Field("Community Area")
    private String communityArea;
    @Field("upvotes")
    private String upvotes;
    @Field("Location")
    private String location;
    @Field("GeoPoint")
    private String geoPoint;

    @PersistenceConstructor
    public Request(String id, Date creationDate, String status,
                   Date completionDate, String serviceRequestNumber, String requestType,
                   String model, String color, String licensePlate,
                   String daysParked, String address, String zipCode, String latitude, String longitude,
                   String ward, String policeDistrict, String communityArea, String upvotes, String location,String geoPoint) {
        this.id = id;
        this.creationDate = creationDate;
        this.status = status;
        this.completionDate = completionDate;
        this.serviceRequestNumber = serviceRequestNumber;
        this.requestType = requestType;
        this.model = model;
        this.color = color;
        this.licensePlate = licensePlate;
        this.daysParked = daysParked;
        this.address = address;
        this.zipCode = zipCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ward = ward;
        this.policeDistrict = policeDistrict;
        this.communityArea = communityArea;
        this.upvotes = upvotes;
        this.location = location;
        this.geoPoint = geoPoint;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCompletionDate() {
        return completionDate;
    }

    public void setCompletionDate(Date completionDate) {
        this.completionDate = completionDate;
    }

    public String getServiceRequestNumber() {
        return serviceRequestNumber;
    }

    public void setServiceRequestNumber(String serviceRequestNumber) {
        this.serviceRequestNumber = serviceRequestNumber;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getDaysParked() {
        return daysParked;
    }

    public void setDaysParked(String daysParked) {
        this.daysParked = daysParked;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getPoliceDistrict() {
        return policeDistrict;
    }

    public void setPoliceDistrict(String policeDistrict) {
        this.policeDistrict = policeDistrict;
    }

    public String getCommunityArea() {
        return communityArea;
    }

    public void setCommunityArea(String communityArea) {
        this.communityArea = communityArea;
    }

    public String getUpvotes() {
        return upvotes;
    }

    public void setUpvotes(String upvotes) {
        this.upvotes = upvotes;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGeoPoint() {
        return geoPoint;
    }

    public void setGeoPoint(String geoPoint) {
        this.geoPoint = geoPoint;
    }
}
