package com.chicago.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Document(collection = "user_data")
public class User {


    @Id
    public String id;
    @Field("name")
    private String name;
    @Field("address")
    private String address;
    @Field("phone_number")
    private String phoneNumber;
    @Field("upvoted_requests")
    private List<Request> upvotedRequests;

    @PersistenceConstructor
    public User(String id, String name, String address, String phoneNumber, List<Request> upvotedRequests) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.upvotedRequests = upvotedRequests;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<Request> getUpvotedRequests() {
        return upvotedRequests;
    }

    public void setUpvotedRequests(List<Request> upvotedRequests) {
        this.upvotedRequests = upvotedRequests;
    }
}
