package com.chicago.controllers;

import com.chicago.domain.Request;
import com.chicago.domain.User;
import com.chicago.service.UserRepository;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/user")
@Api(description = "User API",  tags = {"user"})
public class UserController {

    @Autowired
    UserRepository userRepository;

    @RequestMapping(value = "/add" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    User add(@RequestBody User user) {
        return userRepository.add(user);
    }


}
