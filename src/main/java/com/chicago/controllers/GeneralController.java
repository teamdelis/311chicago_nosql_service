//package com.chicago.controllers;
//
//import com.chicago.domain.*;
//import com.chicago.service.GeneralServiceApi;
//import com.chicago.service.LoggingService;
//import io.swagger.annotations.Api;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.List;
//
//@RestController
//@Api(description = "General API",  tags = {"general"})
//public class GeneralController {
//
//    @Autowired
//    GeneralServiceApi generalService;
//
//    @Autowired
//    LoggingService loggingService;
//
//    @RequestMapping(value = "/streetName/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllStreetNames() {
//        return generalService.getAll(StreetNames.class);
//    }
//
//    @RequestMapping(value = "/zipCode/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllZipCodes() {
//        return generalService.getAll(ZipCodes.class);
//    }
//
//    @RequestMapping(value = "/vehicleMakeModels/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllModels() {
//        return generalService.getAll(VehicleMakeModel.class);
//    }
//
//    @RequestMapping(value = "/vehicleColors/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllColors() {
//        return generalService.getAll(VehicleColor.class);
//    }
//
//    @RequestMapping(value = "/graffitiSurfaces/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllSurfaces() {
//        return generalService.getAll(SurfacesWithGraffiti.class);
//    }
//
//    @RequestMapping(value = "/graffitiStructures/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllStructures() {
//        return generalService.getAll(StructuresWithGraffiti.class);
//    }
//
//    @RequestMapping(value = "/sanitationCodeViolations/getAll" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getAllCodeViolations() {
//        return generalService.getAll(SanitationCodeViolations.class);
//    }
//
//    @RequestMapping(value = "/activities" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getActivitiesByType(@RequestParam("type") String type) {
//        return generalService.getByType(ActivitiesPerType.class,type,"activity");
//    }
//
//    @RequestMapping(value = "/actions" , method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    List getActionsByType(@RequestParam("type") String type) {
//        return generalService.getByType(ActionsPerType.class,type,"action");
//    }
//
//}
