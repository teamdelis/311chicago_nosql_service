package com.chicago.controllers;

import com.chicago.domain.Request;
import com.chicago.exception.ResourceNotFoundException;
import com.chicago.service.RequestRepository;
import io.swagger.annotations.Api;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(value = "/request")
@Api(description = "Request API",  tags = {"request"})
public class RequestController {


    @Autowired
    RequestRepository requestService;


    @RequestMapping(value = "/getByID/{id}" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Request getRequestByID(@PathVariable("id")  String id) throws ResourceNotFoundException {
        return requestService.getRequestById(id);
    }

    @RequestMapping(value = "/upvote" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    void update(@RequestParam("userId") String userId,
                   @RequestParam("requestId") String requestId) {
       requestService.update(userId,requestId);
    }

    @RequestMapping(value = "/add" , method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Request add(@RequestBody Request request) {
        return requestService.add(request);
    }

    @RequestMapping(value = "/query1" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query1(@RequestParam("startDate") String startDate,
                        @RequestParam("endDate") String endDate) {
        return requestService.query1(startDate,endDate);
    }

    @RequestMapping(value = "/query2" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query2(@RequestParam("type") String type,
                        @RequestParam("startDate") String startDate,
                        @RequestParam("endDate") String endDate) {
        return requestService.query2(type,startDate,endDate);
    }

    @RequestMapping(value = "/query3" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query3(@RequestParam("date") String date){
        return requestService.query3(date);
    }


    @RequestMapping(value = "/query4" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query4(@RequestParam("type") String type) {
        return requestService.query4(type);
    }

    @RequestMapping(value = "/query5" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query5(@RequestParam("startDate") String startDate,
                        @RequestParam("endDate") String endDate)  {
        return requestService.query5(startDate,endDate);
    }

    @RequestMapping(value = "/query6" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query6(@RequestParam("x1") Float[] x1,
                        @RequestParam("x2") Float[] x2,
                        @RequestParam("x3") Float[] x3,
                        @RequestParam("x4") Float[] x4,
                        @RequestParam("date") String date) {
        return requestService.query6(x1,x2,x3,x4,date);
    }


    @RequestMapping(value = "/query7" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query7(@RequestParam("date") String date) {
        return requestService.query7(date);
    }

    @RequestMapping(value = "/query8" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query8()  {
        return requestService.query8();
    }


    @RequestMapping(value = "/query9" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query9()  {
        return requestService.query9();
    }

    @RequestMapping(value = "/query10" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query10()  {
        return requestService.query10();
    }

    @RequestMapping(value = "/query11" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query11()  {
        return requestService.query11();
    }

    @RequestMapping(value = "/query12" , method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    List<Object> query12(@RequestParam("name") String name) throws ParseException {
        return requestService.query12(name);
    }



}
